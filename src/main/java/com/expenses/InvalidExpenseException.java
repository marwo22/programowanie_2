package com.expenses;

public class InvalidExpenseException extends Exception {
    public InvalidExpenseException (String msg){
        super(msg);
    }

    public InvalidExpenseException() {

    }
}
