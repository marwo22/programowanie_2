package com.expenses;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ExpenseService {
    private final Set<Expense> expenses = new HashSet<>();

    public void addExpense(Expense expense) {
        expenses.add(expense);
    }

    public Set<Expense> getExpenses() {
        return new HashSet<>(expenses);
    }

    public Set<Expense> findByDate(LocalDate date) {
        Set<Expense> expensesWithRequestedDate = new HashSet<>();

        for (Expense expense : expenses) {
            if (expense.getDate().equals(date)) {
                expensesWithRequestedDate.add(expense);
            }
        }

        return expensesWithRequestedDate;
    }

    // Uzycie Predicate jako oddzielnej klasy z for each
    public Set<Expense> findExpensesBefore(LocalDate date) {
        Set<Expense> expensesBeforeDate = new HashSet<>();

        ExpenseIsBeforeDatePredicate beforeDatePredicate = new ExpenseIsBeforeDatePredicate(date);
        for (Expense expense : expenses) {
            if (beforeDatePredicate.test(expense)) {
                expensesBeforeDate.add(expense);
            }
        }

        return expensesBeforeDate;
    }

    // Uzycie Predicate jako oddzielnej klasy ze streamem
    public Set<Expense> findExpensesBeforeStream(LocalDate date) {
        ExpenseIsBeforeDatePredicate beforeDatePredicate = new ExpenseIsBeforeDatePredicate(date);

        Set<Expense> expensesBeforeDate = expenses.stream()
                .filter(beforeDatePredicate)
                .collect(Collectors.toSet());

        return expensesBeforeDate;
    }

    public Set<Expense> findExpensesBeforeStreamAnonymousClas(LocalDate date) {
        Predicate<Expense> beforeDatePredicate = new Predicate<Expense>() {
            @Override
            public boolean test(Expense expense) {
                return expense.getDate().isBefore(date);
            }
        };

        return expenses.stream()
                .filter(beforeDatePredicate)
                .collect(Collectors.toSet());
    }

    public Set<Expense> findExpensesBeforeStreamLambda(LocalDate date) {
        Predicate<Expense> beforeDatePredicate = expense -> expense.getDate().isBefore(date);

        return expenses.stream()
                .filter(beforeDatePredicate)
                .collect(Collectors.toSet());
    }

    public Set<Expense> findExpensesInDateRange(LocalDate from, LocalDate to) {
        Predicate<Expense> afterDatePredicate = expense -> expense.getDate().isAfter(from);
        Predicate<Expense> beforeDatePredicate = expense -> expense.getDate().isBefore(to);

        return expenses.stream()
                .filter(beforeDatePredicate.and(afterDatePredicate))
                .collect(Collectors.toSet());
    }

    // todo To samo, tylko z wyszukiwaniem po kwocie albo implementacja Comparator
    // todo zastosowac to w findNLargestExpenses

    public String toString() {
        StringBuilder message = new StringBuilder("Expenses:\n");

        for (Expense expense : expenses) {
            message.append(expense).append("\n");
        }

        return message.toString();
    }
}
