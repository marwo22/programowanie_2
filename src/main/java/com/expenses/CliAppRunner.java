package com.expenses;

import com.expenses.io.FileType;

import java.io.InputStream;
import java.io.PrintStream;

public class CliAppRunner {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        PrintStream printStream = System.out;
        ExpenseService expenseService = new ExpenseService();

        ExpenseCliApp expenseCliApp = new ExpenseCliApp(inputStream, printStream, expenseService);
        expenseCliApp.run("data.csv", FileType.CSV);
    }
}
