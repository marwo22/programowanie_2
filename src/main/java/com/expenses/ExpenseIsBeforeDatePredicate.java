package com.expenses;

import java.time.LocalDate;
import java.util.function.Predicate;

public class ExpenseIsBeforeDatePredicate implements Predicate<Expense> {

    private final LocalDate comparedDate;

    public ExpenseIsBeforeDatePredicate(LocalDate comparedDate) {
        this.comparedDate = comparedDate;
    }

    @Override
    public boolean test(Expense expense) {
        return expense.getDate().isBefore(comparedDate);
    }
}
