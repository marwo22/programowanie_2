package com.expenses.currency;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class OnlineCurrencyService implements CurrencyService {
    private static final String NBP_URI_PATTERN = "https://api.nbp.pl/api/exchangerates/rates/A/%s/last/1?format=json";

    @Override
    public BigDecimal convertToPln(BigDecimal amount, Currency currency) {
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(getUri(currency))
                .GET()
                .build();

        HttpResponse<String> response;
        try {
            response = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            throw new CurrencyConversionFailedException("Could not get answer from NBP API: " + e.getMessage());
        }

        String responseBody = response.body();

        JSONObject jsonObject = new JSONObject(responseBody);
        JSONArray jsonArray = jsonObject.getJSONArray("rates");

        if (jsonArray.isEmpty()) {
            throw new CurrencyConversionFailedException("Error getting rate from NBP API response: [" + responseBody + "]");
        }

        JSONObject rateObject = jsonArray.getJSONObject(0);
        BigDecimal rate = rateObject.getBigDecimal("mid");

        return amount.multiply(rate).setScale(2, RoundingMode.HALF_UP);
    }

    private static URI getUri(Currency currency) {
        String currencyUri = String.format(NBP_URI_PATTERN, currency.getCurrencyCode());
        return URI.create(currencyUri);
    }

    public static void main(String[] args) throws Exception {
        String uri = "https://api.nbp.pl/api/exchangerates/rates/A/eur/2020-04-23?format=json";

        HttpClient client = HttpClient.newHttpClient();

        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .GET()
                .build();

        HttpResponse<String> response = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        System.out.println(response.body());
    }
}
