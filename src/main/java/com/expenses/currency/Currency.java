package com.expenses.currency;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.StreamSupport;

public enum Currency {
    US_DOLLAR("USD"),
    BRITISH_POUND("GBP"),
    EURO("EUR"),
    POLISH_ZLOTY("PLN");

    private final String currencyCode;

    Currency(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public static Optional<Currency> findByCode(String currencyCode) {
        Currency[] currencies = Currency.values();

        for (Currency currency : currencies) {
            if (currency.getCurrencyCode().equals(currencyCode)) {
                return Optional.of(currency);
            }
        }

        return Optional.empty();
    }

    static Optional<Currency> findByCodeStream(String currencyCode) {
        return Arrays.stream(Currency.values())
                .filter(currency -> currency.getCurrencyCode().equals(currencyCode))
                .findAny();
    }
}
