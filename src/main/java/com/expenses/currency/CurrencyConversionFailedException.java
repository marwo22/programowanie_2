package com.expenses.currency;

public class CurrencyConversionFailedException extends RuntimeException {
    public CurrencyConversionFailedException(String message) {
        super(message);
    }
}
