package com.expenses;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ExpenseApp {
    public static void main(String[] args) throws InvalidExpenseException {
        ExpenseService expenseService = new ExpenseService();
        Expense.from(BigDecimal.valueOf(-1), LocalDate.now(), "Loc", "Cat");

        System.out.println(expenseService);
    }
}
