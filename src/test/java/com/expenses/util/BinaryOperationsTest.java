package com.expenses.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BinaryOperationsTest {
    public static boolean isNotBlank(String text) {
        // short circuiting
        return text != null && !text.isBlank();
    }

    public static boolean isNotBlankBinary(String text) {
        return text != null & !text.isBlank();
    }


    @Test
    void checkBinaryAnd() {
        int four = 4;
        int six = 6;

        System.out.println(Integer.toBinaryString(four));
        System.out.println(Integer.toBinaryString(six));
        System.out.println("--------------------------");
        System.out.println(Integer.toBinaryString(four & six));
    }

    @Test
    void checkBinaryOr() {
        int four = 4;
        int six = 6;

        System.out.println(Integer.toBinaryString(four));
        System.out.println(Integer.toBinaryString(six));
        System.out.println("--------------------------");
        System.out.println(Integer.toBinaryString(four | six));
    }

    @Test
    void checkIsNotBlank() {
        String nullText = null;

        assertFalse(isNotBlank(nullText));
        assertThrows(NullPointerException.class, () -> isNotBlankBinary(nullText));
    }
}
