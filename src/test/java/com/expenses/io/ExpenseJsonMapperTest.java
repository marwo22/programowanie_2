package com.expenses.io;

import com.expenses.Expense;
import org.junit.jupiter.api.Test;

import java.io.StringReader;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static java.math.BigDecimal.valueOf;
import static java.time.Month.APRIL;
import static java.time.Month.MAY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ExpenseJsonMapperTest {
    @Test
    void shouldWriteExpenses() throws Exception {
        // Given
        StringWriter stringWriter = new StringWriter();
        ExpenseJsonMapper expenseJsonMapper = new ExpenseJsonMapper();

        Set<Expense> expenses = Set.of(
                Expense.from(
                        valueOf(100),
                        LocalDate.of(2000, APRIL, 3),
                        "location1",
                        "category1"),
                Expense.from(
                        valueOf(200),
                        LocalDate.of(2010, MAY, 20),
                        "location2",
                        null)
        );

        // When
        expenseJsonMapper.write(expenses, stringWriter);

        // Then
        System.out.println(stringWriter.toString());
    }

    @Test
    void shouldReadExpenses() throws Exception {
        // Given
        ExpenseJsonMapper expenseJsonMapper = new ExpenseJsonMapper();
        String expensesJson = "[ {\n" +
                "  \"amount\" : 100,\n" +
                "  \"date\" : [ 2000, 4, 3 ],\n" +
                "  \"location\" : \"location1\",\n" +
                "  \"category\" : \"category1\"\n" +
                "}, {\n" +
                "  \"amount\" : 200,\n" +
                "  \"date\" : [ 2010, 5, 20 ],\n" +
                "  \"location\" : \"location2\",\n" +
                "  \"category\" : null\n" +
                "} ]";
        Expense expense1 = Expense.from(
                valueOf(100),
                LocalDate.of(2000, APRIL, 3),
                "location1",
                "category1");
        Expense expense2 = Expense.from(
                valueOf(200),
                LocalDate.of(2010, MAY, 20),
                "location2",
                null);

        // When
        Set<Expense> actualExpenses = expenseJsonMapper.read(new StringReader(expensesJson));

        // Then
        assertEquals(2, actualExpenses.size());
        assertTrue(actualExpenses.contains(expense1), "\nSet: " + actualExpenses + "\nshould contain:\n" + expense1);
        assertTrue(actualExpenses.contains(expense2), "\nSet: " + actualExpenses + "\nshould contain:\n" + expense2);
    }
}