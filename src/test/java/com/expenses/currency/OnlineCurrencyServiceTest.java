package com.expenses.currency;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

class OnlineCurrencyServiceTest {
    // Simple test to demonstrate and check the OnlineCurrencyService
    @Disabled
    @Test
    void shouldGetRateFromNbpApi() {
        OnlineCurrencyService onlineCurrencyService = new OnlineCurrencyService();

        BigDecimal tenBucksInPln = onlineCurrencyService.convertToPln(BigDecimal.TEN, Currency.US_DOLLAR);

        System.out.println(tenBucksInPln);
    }
}